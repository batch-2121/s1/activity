package com.zuitt.s1;

import java.util.Scanner;

public class Activity {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = appScanner.nextLine().trim();

        System.out.println("Last Name");
        String lastName = appScanner.nextLine().trim();

        System.out.println("First Subject Grade:");
        Double firstSubject = appScanner.nextDouble();

        System.out.println("Second Subject Grade:");
        Double secondSubject = appScanner.nextDouble();

        System.out.println("Third Subject Grade:");
        Double thirdSubject = appScanner.nextDouble();

        double average = Math.floor((firstSubject + secondSubject + thirdSubject)/3);

        System.out.println("Good day, " + firstName+ " " +lastName);
        System.out.println("Your grade average is: "+ average);
    }
}
